var user = require("./db_connection")
const User = user.User;
const express = require('express');
const router = express.Router();

router.route("/:id")
.get(async (req, res)=>{
    //console.log(req.params)
    let data = await User.find({_id:req.params.id});
     res.json(data);
})

router.route("/")
.get(async (req, res)=>{
    let data = await User.find();
    res.json(data);
})

.post(async (req, res)=>{
    let u1 = new User(req.body);
    let result = await u1.save();
    res.json(result);
})

.put(async (req, res)=>{
    let u_data = await User.updateOne({_id: req.body.id}, {
        $set: {
            name: req.body.name,
            age: req.body.age,
            city: req.body.city,       
        }
    })
    res.json(u_data);
})

.delete(async (req, res)=>{
    let d_rec = await User.deleteOne({_id:req.query.id})
    res.json(d_rec);
})

module.exports = router