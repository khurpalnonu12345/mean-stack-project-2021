var express = require("express")
var app = express()
var jwt = require("jsonwebtoken")

//cors to resolve 'Access-Control-Allow-Origin' error while making http request to API
var cors = require('cors')
app.use(cors())

app.use(express.json())

//modular routing to /user url
var user = require("./user")

function checkAuth(req,res, next){
    if(!req.headers.authorization) return res.status(401).send({message: "No token Provided"})
    
    const token = req.headers.authorization.replace("Bearer ","");
    
    jwt.verify(token,'hiii', function(err){
        if(err) return res.status(401).send({message: "unthorizated user"})
        
        // res.send("authorized user. jwt token is correct")
        next()
    })
    
    // return res.status(500).json({message: "something went wrong"})
}

app.route('/login')
.post((req, res) => {
    if (req.body.username == "admin" && req.body.pwd == "@123") {
        var token = jwt.sign({ u_name: "admin" }, 'hiii')
        res.send({status:true, token: token})
    } else {
        
        res.send({status: false, message: "wrong credentials"})
    }
})

app.use(checkAuth)

app.route('/dashboard')
.get((req,res)=>{
    res.send("dashboard page")
})

app.route('/student')
.get((req,res)=>{
    res.send("student page")
})

app.use("/user", user)

app.listen(8080, () => {
    console.log("listing at 8080...")
})