const mongoose = require("mongoose");

const conn_str = "mongodb://localhost:27017/college"
mongoose.connect(conn_str,  { useNewUrlParser: true, useUnifiedTopology: true })
.then(() => console.log("connected sucessfully"))
.catch((err) => console.log(err));

//schema
const userSchema = new mongoose.Schema({
    name: String,
    age: Number,
    city: String
})

//create collection
const User = new mongoose.model("User", userSchema);

exports.User = User;