import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AdduserComponent } from './user/adduser/adduser.component';
import { EdituserComponent } from './user/edituser/edituser.component';
import { UserComponent } from './user/user.component';
import {AuthGuard} from './authguard'

const routes: Routes = [
  {path: "user", component:UserComponent, canActivate: [AuthGuard]},
  {path: "user/add", component:AdduserComponent, canActivate: [AuthGuard]},
  {path: "user/edit/:id", component:EdituserComponent, canActivate: [AuthGuard]},
  {path: "login", component:LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
