import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }

  getUsers(){
    let url = `http://localhost:8080/user/`;
    return this.http.get(url);
  }

  getUser(id:String){
    let url = `http://localhost:8080/user/${id}`;
    return this.http.get(url);
  }

  postUser(data:any){
    data.age = parseInt(data.age);
    let url = `http://localhost:8080/user`;
    return this.http.post(url, data);
  }


  deleteUser(id:any){
    let url = `http://localhost:8080/user/?id=${id}`;
    return this.http.delete(url);
  }
  
  putUser(data:any){
    data.age = parseInt(data.age);
    let url = `http://localhost:8080/user`;
    return this.http.put(url, data);
  }
  
}
