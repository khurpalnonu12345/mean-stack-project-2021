import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  usersdata: any;
  d_rec: any;
  constructor(private service:UserService) { }

  ngOnInit(): void {
    this.service.getUsers().subscribe((response)=>{
      console.log(response)
      this.usersdata = response;
    })
  }

  deleteUser(id:any,name:any){
    confirm(`Are you sure you want to delete ${name}`);
    this.service.deleteUser(id).subscribe((response)=>{
      this.d_rec = response;
      if(!this.d_rec){
        alert(`Something went wrong!`)
        return false
      }

      alert(`${name} was deleted sucessfully`);
      window.location.reload();
    })
  }

}
