import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  loggedin: boolean = localStorage.getItem("jwttoken") ? true : false
  constructor() { }

  ngOnInit(): void {
  }

  logout(){
    localStorage.removeItem("jwttoken");
    window.location.href = "/"
  }

}
